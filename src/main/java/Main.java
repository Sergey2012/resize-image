import java.io.File;
import java.util.Objects;

public class Main {
    public static void main(String[] args) {
        int newWidth = 300;
        String srcFolder = "C:/src";
        String dstFolder = "C:/dst";

        File srcDir = new File(srcFolder);

        File[] files = srcDir.listFiles();
        int middle = Objects.requireNonNull(files).length/2;

        File[] files1 = new File[middle];
        System.arraycopy(files,0,files1,0,files1.length);
        ImageResizer resizer1 = new ImageResizer(files1, newWidth,dstFolder);
        resizer1.start();

        File[] files2 = new File[files.length - middle];
        System.arraycopy(files,middle,files2,0,files2.length);
        ImageResizer resizer2 = new ImageResizer(files2, newWidth,dstFolder);
        resizer2.start();
    }
}
